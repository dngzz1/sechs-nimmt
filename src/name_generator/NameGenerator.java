package name_generator;

public class NameGenerator {
	public static final String[] SYLLABARIES = ("A,I,U,E,O,"+
												"KA,KI,KU,KE,KO,"+
												"GA,GI,GU,GE,GO,"+
//												"GYA,GYU,GYO,"+
//												"KYA,KYU,KYO,"+
												"SA,SHI,SU,SE,SO,"+
												"SHA,SHU,SHO,"+
												"ZA,JI,ZU,ZE,ZO,"+
												"TA,CHI,TSU,TE,TO,"+
												"CHA,CHU,CHO,"+
												"DA,DE,DO,"+
												"NA,NI,NU,NE,NO,"+
//												"NYA,NYU,NYO,"+
												"HA,HI,HU,HE,HO,"+
//												"HYA,HYU,HYO,"+
												"MA,MI,MU,ME,MO,"+
//												"MYA,MYU,MYO,"+
												"BA,BI,BU,BE,BO,"+
//												"BYA,BYU,BYO,"+
												"PA,PI,PU,PE,PO,"+
//												"PYA,PYU,PYO,"+
												"YA,YU,YO,"+
												"RA,RI,RU,RE,RO,"+
//												"RYA,RYU,RYO,"+
												"WA").split(",");
	
	public static String getRandomName() {
		String name = "";
		for (int i=0; i < 4; i++) {
			name += SYLLABARIES[randInt(0,SYLLABARIES.length-1)];
		}
		name += " ";
		for (int i=0; i < 4; i++) {
			name += SYLLABARIES[randInt(0,SYLLABARIES.length-1)];
		}
		return toTitleCase(name);
	}
	
	
	
	public static int randInt(int max){
		return (int)(Math.random()*max);
	}
	
	public static int randInt(int min, int max){
		return randInt(max + 1 - min) + min;
	}
	
	public static String toTitleCase(String text) {
		if (text == null || text.isEmpty()) {
	        return text;
	    }

	    StringBuilder converted = new StringBuilder();

	    boolean convertNext = true;
	    for (char ch : text.toCharArray()) {
	        if (Character.isSpaceChar(ch)) {
	            convertNext = true;
	        } else if (convertNext) {
	            ch = Character.toTitleCase(ch);
	            convertNext = false;
	        } else {
	            ch = Character.toLowerCase(ch);
	        }
	        converted.append(ch);
	    }

	    return converted.toString();
	}
	
}
