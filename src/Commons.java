/*
 * A collection of final static variables, according to the game rules.
 */
public class Commons {
	public static final int NUM_ROWS = 4;
	public static final int CARDS_PER_PLAYER = 10;
	public static final int TOTAL_NUM_CARDS = 104;
}
