import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import name_generator.NameGenerator;


public class Player implements Comparable<Player>{
	protected Deck cards = new Deck();
	private static int count = 0;
	private int id;
	private String name;
	private List<Integer> scores = new LinkedList<Integer>();
	
	
	/*
	 * Constructor.
	 */
	public Player() {
		this.id = count++; // automatically add id.
		this.name = NameGenerator.getRandomName();
	}
	
	public Player(String name) {
		this.id = count++;
		this.name = name;
	}
	
	/*
	 * Add a card to a player's hand.
	 */
	public void addCard(Card card) {
		card.setOwner(this);
		cards.add(card);
		cards.sort();
	}
	
	/*
	 * Add several cards to a player's hand.
	 */
	public void addCards(List<Card> newCards) {
		for (Card card : newCards) {
			addCard(card);
		}
		cards.sort();
	}
	
	
	/*
	 * When the player places a card whose value is too small to be added
	 * to any row, then ask the player which row to pick up.
	 */
	public int chooseRowToPickUp(List<List<Card>> rows) {
		// find all rows with max number of points.
		int[] points = new int[rows.size()];
		int max_point = 0;
		for (int i = 0; i < rows.size(); i++) {
			for (int j = 0; j < rows.get(i).size(); j++) {
				points[i] += rows.get(i).get(j).getPoint();
			}
			max_point = Math.max(max_point, points[i]);
		}
		int choose_this_row = 0;
		for (int i = 0; i < rows.size(); i++) {
			if (points[i] == max_point) {
				choose_this_row = i; // choose first row with max point.
			}
		}
		return choose_this_row;
	}
	
	/*
	 * Player takes a card and signs it with their name.
	 */
	public void draw(Deck deck) {
		Card drawn_card = deck.draw();
		drawn_card.setOwner(this);
		cards.add(drawn_card);
		cards.sort();
	}
	
	/*
	 * Empty the player's hand.
	 */
	public void emptyHand() {
		cards.clear();
	}
	
	/*
	 * This method is called when the player is asked to put down 
	 * a card at the beginning of a turn.
	 */
	public Card playCard(List<List<Card>> rows) {
		// if possible to place consecutively, do it!
		List<Card> options = new LinkedList<Card>();
		for (int i = 0; i < rows.size(); i++) {
			List<Card> ith_row = rows.get(i);
			if (ith_row.size() < 5) {
				options.add(ith_row.get(ith_row.size()-1));
			}
		}
		for (int i = 0; i < options.size(); i++) {
			for (int j = 0; j < cards.getSize(); j++) {
				if (options.get(i).getValue() + 1 == cards.get(j).getValue()) {
					return cards.remove(j);
				}
			}
		}
		// otherwise play randomly.
		return playCard();
	}
	
	/*
	 * Plays a random card when asked to put down a card with no extra info given.
	 */
	public Card playCard() {
		int randomNum = ThreadLocalRandom.current().nextInt(0, cards.getSize());
		return cards.remove(randomNum);
	}
	
	/*
	 * Sorts the deck in ascending card value.
	 */
	public void reorderCards() {
		cards.sort();
	}
	
	/*
	 * Getters.
	 */
	public Deck getCards() {
		return cards;
	}
	
	public int getId() {
		return id;
	}
	
	public String getInfo() {
		return "Player " + id + ", " + name + ": cards = " + cards 
				+ ", total score = " + getTotalScore();
	}
	
	public String getName() {
		return name;
	}
	
	public int getHandScore() {
		return cards.getScore();
	}
	
	public List<Integer> getScores() {
		return scores;
	}
	
	public int getTotalScore() {
		int result = 0;
		for (int s : scores) {
			result += s;
		}
		return result;
	}

	/*
	 * Setters.
	 */
	public void addToScore() {
		scores.add(cards.getScore());
	}
	@Override
	public String toString() {
		return name + "(id=" + id + ")";
	}

	@Override
	public int compareTo(Player o) {
		// TODO Auto-generated method stub
		return this.getHandScore() - o.getHandScore();
	}

	

	
}



