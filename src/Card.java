

public class Card implements Comparable<Card>{
	private int value;
	private Player owner;
	private int point;
	
	public Card(int value) {
		this.value = value;
		this.point = pointCalculator(value);
	}
	
	private int pointCalculator(int value) {
		int point = -1;
		if (value % 10 == 5) {
			point = -2;
		}
		else if (value % 10 == 0) {
			point = -3;
		}
		
		if (value % 11 == 0) {
			point = -5;
		}
		
		if (value == 55) {
			point = -7;
		}
		return point;
	}
	
	/*
	 * Two cards are equal if they have the same value.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (value != other.value)
			return false;
		return true;
	}
	

	/*
	 * Getters and setters.
	 */
	
	public int getValue() {
		return value;
	}
	public int getPoint() {
		return point;
	}
	public Player getOwner() {
		return owner;
	}
	
	public void setOwner(Player owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return String.format("%03d", value);
	}
	
	/*
	 * Comparable.
	 */
	public int compareTo(Card c) {
		return this.value - c.value;
	}
	

}
