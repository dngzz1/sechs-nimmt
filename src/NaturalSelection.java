import java.util.ArrayList;

public class NaturalSelection {
	ArrayList<Player> players = new ArrayList<Player>();
	private int num_players; // BETWEEN 4 and 10 inclusive.
	private int num_rounds = 100;
	
	/*
	 * Constructors.
	 */
	public NaturalSelection(int num_players) {
		if (num_players < 2 || num_players > 10) {
			System.out.println("Number of players must be between 2 and 10 inclusive.");
		} else {
			this.num_players = num_players;
			run();
		}
	}
	public void initPlayers() {
		for (int i = 0; i< num_players; i++) {
			players.add(new Player());
		}
	}
	
	/*
	 * Main method.
	 */
	public void run() {
		initPlayers();
		for (int i = 0; i < num_rounds; i++) {
			Arena arena = new Arena(players);
		    arena.run();
		}
	    
    
	    for (Player player : players) {
	    	player.emptyHand();
	    	System.out.println(player.getInfo());
	    }
	}
	
	
	
}
