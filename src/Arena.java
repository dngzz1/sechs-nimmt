
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;




public class Arena{
	
	public int num_players; // BETWEEN 4 and 10 inclusive.
	
	
	
	public boolean game_active = true;
	
	private Deck deck = new Deck();
	private List<Player> players = new ArrayList<Player>();
	private List<List<Card>> rows = new ArrayList<List<Card>>();
	private int turn = 0;
	private StringBuilder history = new StringBuilder();
	private StringBuilder scoreboard = new StringBuilder();

	
	/*
	 * Constructor. Initialises a default number of players and sets up the game.
	 */
	public Arena(int num_players) {
		// creates an arena with randomised players.
		this.num_players = num_players;
		deck.init(Commons.TOTAL_NUM_CARDS);
		initPlayers(num_players);
		dealCards();
		initRows();
    }
	
	/*
	 * Constructor. Initialises a predetermined set of players into the game.
	 */
	public Arena(List<Player> players) {
		deck.init(Commons.TOTAL_NUM_CARDS);
		this.players = players;
		this.num_players = players.size();
		dealCards();
		initRows();
	}
	
	/*
	 * INIT METHODS.
	 */
	public void dealCards() {
		for (int i = 0; i < num_players; i++) {
			players.get(i).emptyHand(); // make sure the player has no hidden cards.
			for (int j = 0; j < Commons.CARDS_PER_PLAYER; j++) {
				players.get(i).draw(deck);
				players.get(i).reorderCards();
			}
		}
	}
	
	public void initPlayers(int numPlayers) {
		for (int i = 0; i< numPlayers; i++) {
			players.add(new Player());
		}
	}
	
	public void initRows() {
		for (int i = 0; i < Commons.NUM_ROWS; i++) {
			rows.add(new ArrayList<Card>());
			rows.get(i).add(deck.draw());
		}
	}
	
	/*
	 * MAIN METHOD. 
	 */
	public void run() {
		while (game_active) {
			step();
		}
	}
	
	public void step() { 
		turn++;
		print_and_append("========\nTURN " + turn + ": ");
		print_and_append(rows_to_string());
		if (players.get(0) instanceof Player0) {
			print_and_append("Your cards: " + players.get(0).cards.toString());
		}
		Deck pot = new Deck();
		for (int i = 0; i < num_players; i++) {
			Card player_i_card = players.get(i).playCard(rows);
			pot.add(player_i_card);
			print_and_append("Player " + i + 
					//" (" + players.get(i).getName() + ")" +
					" played card " + player_i_card.toString() + "!");
		}
		pot.sort();
		print_and_append("Cards to be played are " + pot.toString() + ".");
		merge(pot, rows);
		check_game_active();
	}
	
	/*
	 * Merges the played cards (pot) with the rows.
	 * Most of it is automatic but it may need to ask the player to choose a row to pick up
	 * if the player has placed a card whose value is smaller than everything.
	 */
	public void merge(Deck pot, List<List<Card>> rows) {
		while (pot.getSize() > 0) {
			Card curr_card = pot.draw();
			// check if curr_card can be placed.
			int place_here = -1;
			int curr_max_card_val = 0;
			for (int i = 0; i < Commons.NUM_ROWS; i++) {
				int ith_row_last_card_val = rows.get(i).get(rows.get(i).size()-1).getValue();
				if (ith_row_last_card_val < curr_card.getValue() && ith_row_last_card_val > curr_max_card_val) {
					curr_max_card_val = ith_row_last_card_val;
					place_here = i;
				}
			}
			Player player = curr_card.getOwner();
			if (place_here != -1) {
				// card tries to go to the correct row...
				if (rows.get(place_here).size() < 5) {
					// card okay to go to row.
					rows.get(place_here).add(curr_card);
					print_and_append("Player " + player.getId() + " places card " + 
					curr_card.toString() + " into row " + place_here + ".");
				} else {
					// row is full, player takes the whole row.
					player.addCards(rows.get(place_here));
					rows.get(place_here).clear();
					rows.get(place_here).add(curr_card);
					print_and_append("Player " + player.getId() + 
							" takes entire row " + place_here + 
							" and places card " + curr_card +" there.");
				}
			} else {
				// player takes a row of choice.
				int choose_this_row = player.chooseRowToPickUp(rows);
				player.addCards(rows.get(choose_this_row));
				rows.get(choose_this_row).clear();
				rows.get(choose_this_row).add(curr_card);
				print_and_append("Player " + player.getId() + 
						" takes entire row " + choose_this_row + 
						" and places card " + curr_card +" there.");
				}
		}
	}
	
	/*
	 * If someone has score 0, then end the game.
	 */
	public void check_game_active() {
		for (Player player : players) {
			if (player.getCards().getSize() == 0) {
				game_active = false;
				break;
			}
		}
		// finalise the score data.
		if (!game_active) {
			List<Player> winners = new LinkedList<Player>();
			for (Player player : players) {
				if (player.getCards().getSize() == 0) {
					winners.add(player);
				}
			}
			if (winners.size() > 1) {
				scoreboard.append("Winners are: " + winners + "!\n");
			} else if (winners.size() == 1) {
				scoreboard.append("The winner is " + winners.get(0) + "!\n");
			} else {
				scoreboard.append("There are no winners.\n");
			}
			print_and_append(scoreboard.toString());
			// Save the score.
			for (Player player : players) {
				player.addToScore();
			}
		}
		
	}
	
	/*
	 * Getters.
	 */
	public String getHistory() {
		return history.toString();
	}
	
	public String getScoreboard() {
		return scoreboard.toString();
	}
	
	
	public List<Player> getRanking() {
		return players.stream()
                .sorted((o1,o2) -> o2.compareTo(o1))
                .collect(Collectors.toList());
	}
	/*
	 * Get rows.
	 */
	public String rows_to_string() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Commons.NUM_ROWS; i++) {
			sb.append("Row " + i + ": ");
			for (int j = 0; j < rows.get(i).size(); j++) {
				sb.append(rows.get(i).get(j));
				if (j != rows.get(i).size() - 1) {
					sb.append(",");
				}
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public void print_and_append(String s) {
		history.append(s + "\n");
		System.out.println(s);
	}

	@Override
	public String toString() {
		return history.toString();
	}
	
	
}
