package game_utils;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Math {
	
	public static int randInt(int a, int b) {
		// Returns a random integer in [a,b).
		return ThreadLocalRandom.current().nextInt(a, b);
	}
	
	public static void printPermutations(int[] n, int[] Nr, int idx) {
	    if (idx == n.length) {  //stop condition for the recursion [base clause]
	        System.out.println(Arrays.toString(n));
	        return;
	    }
	    for (int i = 0; i <= Nr[idx]; i++) { 
	        n[idx] = i;
	        printPermutations(n, Nr, idx+1); //recursive invocation, for next elements
	    }
	}

}
