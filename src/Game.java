import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Game {
	public Scanner scanner = new Scanner(System.in);
	public String yourName = "";
	public int num_opponents = 3;
	public List<Player> players = new LinkedList<Player>();
	
	public Game() {
		initUserGame();
	}
	
	public void initAutoGame() {
		for (int i = 0; i < num_opponents + 1; i++) {
			players.add(new Player());
		}
		run();
	}
	
	public void initUserGame() {
		askName();
		Player you = new Player0(yourName, scanner);
		players.add(you);
		for (int i = 0; i < num_opponents; i++) {
			players.add(new Player());
		}
		run();
	}
	
	public void askName() {
		System.out.println("Enter your name: ");
		yourName = scanner.nextLine();
	}
	
	public void run() {
		Arena arena = new Arena(players);
		while (arena.game_active) {
			// System.out.println(arena);
			arena.step();
		}
	}
}
