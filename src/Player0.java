import java.util.List;
import java.util.Scanner;

public class Player0 extends Player {
	Scanner scanner;
	public Player0(String name, Scanner scanner) {
		super(name);
		this.scanner = scanner;
	}
	
	@Override
	public int chooseRowToPickUp(List<List<Card>> rows) {
		// ask user which row to pick up. Does not depend on the input List<List<Card>> rows.
		int choose_this_row;
		do  {
			System.out.println("Choose row to pick up (Enter a number between 0 and " + (Commons.NUM_ROWS - 1) + ": ");
			choose_this_row = scanner.nextInt();
		} while ((choose_this_row < 0 || choose_this_row > Commons.NUM_ROWS - 1));
		return choose_this_row;
	}
	
	@Override
	public Card playCard(List<List<Card>> rows) {
		System.out.println("Enter value of card to be played: ");
		int card_played = -1;
		while (!checkHasCard(card_played)) {
			card_played = scanner.nextInt();
			if (!checkHasCard(card_played)) {
				System.out.println("You don't have that card. \nEnter value of card to be played: ");
			}
		}
		int play_ith_card = -1;
		for (int i = 0; i < cards.getSize(); i++) {
			if (card_played == cards.get(i).getValue()) {
				play_ith_card = i;
			}
		}
		
		return cards.remove(play_ith_card);
	}
	
	public boolean checkHasCard(int val) {
		for (int i = 0; i < cards.getSize(); i++) {
			if (val == cards.get(i).getValue()) {
				return true;
			}
		}
		return false;
	}
	
	
}
