import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class Deck {
	private LinkedList<Card> cards = new LinkedList<Card>();
	
	public Deck() {
		
	}
	
	public Deck(LinkedList<Card> cards) {
		this.cards = cards;
	}
	/*
	 * Methods.
	 */
	public void add(Card card) {
		cards.add(card);
	}
	
	public void add(List<Card> newCards) {
		cards.addAll(newCards);
	}
	
	public void clear() {
		cards.clear();
	}
	
	public Card draw() {
		return cards.pop();
	}
	
	public Card get(int i) {
		return cards.get(i);
	}
	
	public void init(int total_num_cards) {
		for (int i = 1; i < total_num_cards+1; i++) {
			cards.add(new Card(i));
		}
		shuffle();
	}
	
	public Card remove(int i) {
		return cards.remove(i);
	}
	
	public void shuffle() {
		java.util.Collections.shuffle(cards);
	}
	
	public void sort() {
		Collections.sort(cards);
	}
	
	public int getScore() {
		int score = 0;
		for (Card card : cards) {
			score += card.getPoint();
		}
		return score;
	}
	
	public int getSize() {
		return cards.size();
	}
	

	@Override
	public String toString() {
		String result = "[";
		for (int i = 0; i < cards.size(); i++) {
			result += cards.get(i).toString();
			if (i != cards.size()-1) { // don't end list with a comma
				result += ",";				
			}
		}
		result += "]";
		return result;
	}
	
	
}
